#pragma once
#include "Actor.h"

//Abstract map bonus for player taking
class Bonus : public Actor
{
public:
	Bonus(pair<int, int> coords) : Actor(coords)
	{
		
	}

	//Apply bonus effect to player
	virtual void Apply() = 0;
};

