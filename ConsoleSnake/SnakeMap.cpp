#include "SnakeMap.h"

#include <iostream>

using namespace std;

SnakeMap* SnakeMap::instanse = 0;

SnakeMap::SnakeMap()
{
	this->width = 20;
	this->height = 20;
	this->player = SnakePlayer::GetPlayer();
}

SnakeMap* SnakeMap::GetLevel()
{
	if (!instanse) instanse = new SnakeMap();
	return instanse;
}

void SnakeMap::Redraw(const int playerScore)
{
	system("cls");		//Clear console before redrawing

	//Update scene actors vector
	this->sceneActors = Actor::GetAllActors();

	//Get player score
	this->playerScore = playerScore;

	DrawPlayerScore();

	DrawHorizontalBorder();
	for (int i = 1; i < height - 1; i++) DrawAreaRow(i);
	DrawHorizontalBorder();
}

void SnakeMap::DrawPlayerScore()
{
	cout << "Score: " << this->playerScore << endl;
}

void SnakeMap::DrawHorizontalBorder()
{
	cout << " "; //We have empty angles

	//Make border
	for (int i = 1; i < width - 1; i++) 
		cout << "#";

	cout << " " << endl; //We have empty angles
}

void SnakeMap::DrawAreaRow(const int y)
{
	cout << "#";

	for (int i = 1; i < width - 1; i++)
		DrawCell(i, y);

	cout << "#" << endl;
}

void SnakeMap::DrawCell(const int x, const int y)
{
	pair<int, int> coords;

	coords.first = x;
	coords.second = y;

	for (auto i = sceneActors.begin(); i != sceneActors.end(); i++)
	{
		Actor* actor = *i;

		if (IsMapCellBusy(actor, coords))
		{
			cout << actor->renderSymb;
			return;
		}	
	}

	//Empty cell if no busy
	cout << " ";
}

bool SnakeMap::IsMapCellBusy(const Actor* actor, const pair<int, int> coords)
{
	return actor->coords == coords;
}

