#pragma once
#include "Actor.h"

class SnakePlayer : public Actor
{
private:
	static SnakePlayer* instanse;
	SnakePlayer();
	SnakePlayer(SnakePlayer& player) {};

public:
	
	//First is X (right axis), second is Y (up axis)
	pair<int, int> direction;

public:

	//
	static SnakePlayer* GetPlayer();

	//
	void GetInput();

	//
	~SnakePlayer() override 
	{
		delete instanse;
	}

};

