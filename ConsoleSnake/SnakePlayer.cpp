#include "SnakePlayer.h"
#include <conio.h>

SnakePlayer* SnakePlayer::instanse = 0;

SnakePlayer::SnakePlayer()
{
	coords.first = 0;		//X
	coords.second = 0;		//Y
	
	//Default go up
	direction.first = 0;	//X
	direction.second = -1;	//Y

	//This symbol used in level rendering
	this->renderSymb = '0';

}

SnakePlayer* SnakePlayer::GetPlayer()
{
	if (!instanse) instanse = new SnakePlayer();
	return instanse;
}

void SnakePlayer::GetInput()
{
	if (_kbhit())
	{
		int keyCode = _getch();
		
		switch (keyCode)
		{
		case 'w':		//Go up
			direction.first = 0;
			direction.second = -1;
			break;

		case 'a':		//Go left
			direction.first = -1;
			direction.second = 0;
			break;

		case 's':		//Go down
			direction.first = 0;
			direction.second = 1;
			break;

		case 'd':		//Go right
			direction.first = 1;
			direction.second = 0;
			break;

		default:
			break;
		}
	}
}
