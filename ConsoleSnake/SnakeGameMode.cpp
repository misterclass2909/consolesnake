#include "SnakeGameMode.h"
#include "SystemParameters.h"

#include <ctime>
#include <vector>

SnakeGameMode* SnakeGameMode::instanse = 0;

SnakeGameMode::SnakeGameMode()
{
	this->player = SnakePlayer::GetPlayer();
	this->playerScore = 0;
	this->currentFruit = SpawnRandomFruit();
}

SnakeGameMode* SnakeGameMode::GetGameMode()
{
	if (!instanse) instanse = new SnakeGameMode();
	return instanse;
}

void SnakeGameMode::Logic()
{
	MovePlayerLogic();
	EatBonusPlayerLogic();
}

void SnakeGameMode::EatPlayerFruit()
{
	AddPlayerScore(500.f);
	
	//Destroy current fruit
	delete currentFruit;

	SpawnRandomFruit();
}

void SnakeGameMode::AddPlayerScore(const int score)
{
	playerScore += score;
}

Fruit* SnakeGameMode::SpawnRandomFruit()
{
	SystemParameters* params = SystemParameters::GetInstanse();
	pair<int, int> coords;
	pair<int, int> areaSize = params->areaSize;

	srand(time(NULL));

	//Rand from 1 to size - 1 inclusive
	coords.first = rand() % (areaSize.first - 2) + 1;	
	coords.second = rand() % (areaSize.second - 2) + 1;
	
	Fruit* fruit = new Fruit(coords);
	currentFruit = fruit;

	return fruit;
}

void SnakeGameMode::EatBonusPlayerLogic()
{
	vector<Actor*> actors = Actor::GetAllActors();

	for (auto i = actors.begin(); i != actors.end(); i++)
	{
		Bonus* bonus = dynamic_cast<Bonus*>(*i);

		if (bonus && bonus->coords == player->coords)
		{
			bonus->Apply();
			break;
		}
	}
}

void SnakeGameMode::MovePlayerLogic()
{
	pair<int, int> direction = player->direction;

	//Increment player position
	player->coords.first += direction.first;
	player->coords.second += direction.second;
}
