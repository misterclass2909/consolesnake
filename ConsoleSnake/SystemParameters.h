#pragma once
#include <utility>

class SystemParameters
{
	//Singleton
private:

	static SystemParameters* instanse;
	SystemParameters();
	SystemParameters(const SystemParameters& obj) {}

public:
	static SystemParameters* GetInstanse();

public:
	float frameRate;
	std::pair<int, int> playerStartCoords;
	std::pair<int, int> areaSize;
};

