#pragma once
#include "Bonus.h"
class Fruit : public Bonus
{
public:

	Fruit(pair<int, int> coords);

	//Apply bonus effect to player
	void Apply() override;
};

