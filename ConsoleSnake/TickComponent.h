#pragma once
class TickComponent
{
private:
	float frameRate;
	void (*UpdateFunction)(const float);

private:

	//Update using duration
	void UpdateWithFrameRate();

	//Constantable update, every frame
	void UpdateConst();

public:
	TickComponent(const float frameRate);

	//Start event tick
	void StartTick(void (*update)(const float));

	//Call functions with frame rate
	void Update();
	
};

