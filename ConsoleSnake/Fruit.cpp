#include "Fruit.h"
#include "SnakeGameMode.h"


Fruit::Fruit(pair<int, int> coords) : Bonus(coords)
{
	//This symbol used in level rendering
	this->renderSymb = 'F';
}

void Fruit::Apply()
{
	SnakeGameMode* gameMode = SnakeGameMode::GetGameMode();
	gameMode->EatPlayerFruit();
}
