#include "SystemParameters.h"

SystemParameters* SystemParameters::instanse = 0;

SystemParameters::SystemParameters()
{
	frameRate = 5.f;

	areaSize.first = 20;
	areaSize.second = 20;

	//
	playerStartCoords.first = areaSize.first / 2;
	playerStartCoords.second = areaSize.second / 2;
}

SystemParameters* SystemParameters::GetInstanse()
{
	if (!instanse) instanse = new SystemParameters();
	return instanse;
}
