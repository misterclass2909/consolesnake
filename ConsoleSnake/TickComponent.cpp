#include "TickComponent.h"
#include <thread>
#include <chrono>

//Create float duration for correct frame rate
using namespace std::chrono;
using fseconds = duration<float>;

TickComponent::TickComponent(const float frameRate)
{
	this->frameRate = frameRate;
	this->UpdateFunction = NULL;
}

void TickComponent::StartTick(void(*update)(const float))
{
	this->UpdateFunction = update;
}

void TickComponent::Update()
{
	if (this->UpdateFunction)
	{
		if (frameRate != 0.f) UpdateWithFrameRate();
		else UpdateConst();
	}
}

void TickComponent::UpdateWithFrameRate()
{
	//Get frame time in seconds
	float seconds = 1.0f / frameRate;

	//Waiting for update
	std::this_thread::sleep_for(fseconds(seconds));

	this->UpdateFunction(frameRate);
}

void TickComponent::UpdateConst()
{
	this->UpdateFunction(frameRate);
}
