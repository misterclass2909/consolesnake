#include "SnakeProject.h"
#include <chrono>
#include <thread>

SnakeProject* SnakeProject::instanse = 0;

void TickLogicApply(const float frameRate)
{
	SnakeProject* obj = SnakeProject::GetInstanse();

	int playerScore = obj->currentGameMode->playerScore;
	obj->currentLevel->Redraw(playerScore);

	obj->currentPlayer->GetInput();

	obj->currentGameMode->Logic();
}

SnakeProject::SnakeProject()
{
	InitGameParameters();
	InitGameComponents();
}

SnakeProject* SnakeProject::GetInstanse()
{
	if (!instanse) instanse = new SnakeProject();
	return instanse;
}

void SnakeProject::InitGameComponents()
{
	currentGameMode = SnakeGameMode::GetGameMode();

	currentLevel = SnakeMap::GetLevel();

	currentPlayer = SnakePlayer::GetPlayer();
	currentPlayer->coords = playerStartCoords;

	tickComponent = new TickComponent(frameRate);
}

void SnakeProject::InitGameParameters()
{
	params = SystemParameters::GetInstanse();

	isGameOver = false;
	frameRate = params->frameRate;

	//TODO: Bind area size with map class
	areaSize = params->areaSize;

	playerStartCoords = params->playerStartCoords;
}


void SnakeProject::Start()
{
	SetupTick();
}

void SnakeProject::SetupTick()
{
	void (*UpdateFunction)(const float) = &TickLogicApply;
	tickComponent->StartTick(UpdateFunction);

	while (!isGameOver)
	{
		tickComponent->Update();
	}

	Finish();
}

void SnakeProject::Finish()
{
	delete currentGameMode;
	delete currentPlayer;
	delete currentLevel;
	delete tickComponent;
}


