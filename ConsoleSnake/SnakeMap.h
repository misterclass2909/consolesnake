#pragma once
#include <utility>
#include "Actor.h"
#include "SnakePlayer.h"

class SnakeMap
{
private:
	static SnakeMap* instanse;
	SnakeMap();
	SnakeMap(const SnakeMap& map) {};

private:
	SnakePlayer* player;
	vector<Actor*> sceneActors;
	int playerScore;

public:
	int width;
	int height;	

public:

	//Get current sinleton level
	static SnakeMap* GetLevel();

	//Map redrawing in console
	void Redraw(const int playerScore);

	~SnakeMap(){}

private:

	void DrawPlayerScore();
	
	//Draw map border (up and down)
	void DrawHorizontalBorder();

	//Draw one area row (with left and right borders, actors)
	void DrawAreaRow(const int y);

	//Draw one cell with X and Y coords
	void DrawCell(const int x, const int y);

	//Is map cell busy by actor
	bool IsMapCellBusy(const Actor* actor, const pair<int, int> coords);
};

