#pragma once
#include <utility>
#include <vector>

using namespace std;

class Actor
{

private:
	static vector<Actor*> sceneActors;

public:

	//First is X, second is Y coords
	pair<int, int> coords;

	//Symbol for rendering in console
	char renderSymb;

public:

	Actor();
	
	//Copy constructuble
	Actor(const Actor& obj);

	Actor(pair<int, int> coords);

	//Copy assignable
	Actor& operator=(const Actor& obj);

	virtual ~Actor() = 0;

public:
	
	//Return all actors in scene
	static vector<Actor*> GetAllActors();
};

