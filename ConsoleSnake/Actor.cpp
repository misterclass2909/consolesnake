#include "Actor.h"
#include <algorithm>

//Static values must be defined!
vector<Actor*> Actor::sceneActors = vector<Actor*>();

Actor::Actor()
{
	sceneActors.push_back(this);
	this->coords.first = this->coords.second = 0;
}

Actor::Actor(const Actor& obj)
{
	sceneActors.push_back(this);
	this->coords = obj.coords;
}

Actor::Actor(pair<int, int> coords)
{
	sceneActors.push_back(this);
	this->coords = coords;
}

Actor& Actor::operator=(const Actor& obj)
{
	//Detect self-assignment
	if (this == &obj) return *this;

	this->coords = obj.coords;

	return *this;
}

Actor::~Actor()
{
	auto begin = sceneActors.begin();
	auto end = sceneActors.end();

	//Remove all pointers to this element
	sceneActors.erase(std::remove(begin, end, this), end);
}

vector<Actor*> Actor::GetAllActors()
{
	return sceneActors;
}
