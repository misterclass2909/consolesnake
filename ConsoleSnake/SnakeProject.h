#pragma once

#include "SnakeGameMode.h"
#include "SnakeMap.h"
#include "SnakePlayer.h"
#include "TickComponent.h"
#include "SystemParameters.h"

#include <utility>

class SnakeProject
{

//Singleton options
private:
	static SnakeProject* instanse;
	TickComponent* tickComponent;
	SnakeProject();
	SnakeProject(const SnakeProject& project) {};

	//Singleton method
public:
	static SnakeProject* GetInstanse();

//Game components
public:
	SnakeGameMode* currentGameMode;
	SnakePlayer* currentPlayer;
	SnakeMap* currentLevel;
	SystemParameters* params;

//System parameters
public:
	bool isGameOver;
	float frameRate;
	std::pair<int, int> playerStartCoords;
	std::pair<int, int> areaSize;

public:
	
	//Init game, parameters
	void Start();

	//
	void Finish() ;

//Private functions
private:
	void InitGameComponents();
	void InitGameParameters();

	//Start tick logic calculation
	void SetupTick();

	//Call all game logic in one tick
	friend void TickLogicApply(const float frameRate);
};

