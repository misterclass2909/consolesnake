#pragma once
#include "SnakePlayer.h"
#include "Fruit.h"

//
class SnakeGameMode
{
private:
	static SnakeGameMode* instanse;
	SnakeGameMode();
	SnakeGameMode(SnakeGameMode& gameMode) {};

public:

	int playerScore;

public:

	static SnakeGameMode* GetGameMode();

	//Apply game logic with rules, input, game over etc.
	void Logic();

	//
	void EatPlayerFruit();

	//
	~SnakeGameMode(){}

private:
	SnakePlayer* player;
	Fruit* currentFruit;

private:

	//Increment player position by his direction
	void MovePlayerLogic();

	//Increment player score in game
	void AddPlayerScore(const int score);

	//Spawn fruit with random coords
	Fruit* SpawnRandomFruit();

	//
	void EatBonusPlayerLogic();
};

